<%--
  #%L
  pos-service
  %%
  Copyright (C) 2020 Fryske Akademy
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Grapheme to phoneme Service for Frisian</title>
        <style type="text/css">
            td { padding: 4px; border: 1px solid lightgray }
        </style>
    </head>
    <body>
        <h1>Home of Fryske Akademy Grapheme to phoneme Service</h1>

        <p><a href="rest/graph2phon/info">service info</a></p>
        <p>You can post plain text to "rest/graph2phon/process" and get phonemes back in tsv.</p>
        <p>See also the <a href="https://fryske-akademy.nl/fa-apps/g2p/">web-application</a> </p>
        <p>tsv format is:</p>
        <table><tr>
            <td>graphemic</td>
            <td>phonemic</td>
            <td>xsampa</td>
        </tr></table>

        <p>There is a maximum size, see <a href="rest/graph2phon/info">info</a>, for bigger texts use <a href="https://bitbucket.org/fryske-akademy/grapheme2phoneme">command line script</a>.</p>

        <form action="rest/graph2phon/process" method="post" enctype="text/plain">
            <textarea rows="5" cols="80" name="text">
                Dit is in fryske tekst dy't ûndersocht wurde kin
            </textarea>
            <input type="submit" value="phonemes"/>
        </form>

        <p>Contact: e drenth at fryske-akademy dot nl</p>
        <p><a href="https://bitbucket.org/fryske-akademy/grapheme2phoneme/issues">Issues</a></p>
    </body>
</html>
