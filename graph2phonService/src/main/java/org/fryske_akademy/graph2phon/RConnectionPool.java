package org.fryske_akademy.graph2phon;

/*-
 * #%L
 * pos-service
 * %%
 * Copyright (C) 2020 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.configuration.cdi.Property;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

@ApplicationScoped
public class RConnectionPool {

    public static final String STARTRSERVE = "R CMD Rserve --no-save --slave";
    public static final String LOAD_R = "load.R";
    public static final String DICT = "/opt/list.xlsx";

    private static final Logger LOGGER = Logger.getLogger(RConnectionPool.class.getName());

    @Inject
    @Property(defaultValue = {"/bin/sh","-c", STARTRSERVE})
    private String[] startRserve;

    @Inject
    @Property(defaultValue = "3")
    private int poolSize;
    @Inject
    @Property(defaultValue = "-1")
    private int poolTimeoutMillis;

    @Context
    private ServletContext servletContext;

    private final GenericObjectPoolConfig<WrappedRConnection> config = new GenericObjectPoolConfig<>();

    private ObjectPool<WrappedRConnection> rConnectionObjectPool;

    @PostConstruct
    private void init() {
        try {
            String scriptDir = servletContext.getRealPath(Graph2PhonService.SCRIPTDIR);
            new ProcessBuilder(startRserve).start().waitFor();
            config.setMaxTotal(poolSize);
            config.setMaxIdle(poolSize);
            config.setMaxWaitMillis(poolTimeoutMillis);
            rConnectionObjectPool = new GenericObjectPool<WrappedRConnection>(
                    new RconnectionFactory(DICT,scriptDir+ File.separator+LOAD_R),
                    config);
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE,"error loading",ex);
            throw new IllegalStateException(ex);
        }
    }

    @PreDestroy
    private void stop() {
        try {
            rConnectionObjectPool.borrowObject().shutdown();
        } catch (Exception e) {
            throw new IllegalStateException("shutdown Rserve failed", e);
        }
    }

    public WrappedRConnection borrow() throws Exception {
        return rConnectionObjectPool.borrowObject();
    }

    public void putBack(WrappedRConnection rConnection) throws Exception {
        rConnectionObjectPool.returnObject(rConnection);
    }

}
