package org.fryske_akademy.graph2phon;

/*-
 * #%L
 * pos-service
 * %%
 * Copyright (C) 2020 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPString;
import org.rosuda.REngine.Rserve.RserveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletContext;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Properties;
import java.util.UUID;

@Stateless
@Path(Graph2PhonService.ROOT)
public class Graph2PhonService {

    public static final String ROOT = "graph2phon";
    public static final String INFOPATH = "info";
    private static final Logger LOGGER = LoggerFactory.getLogger(Graph2PhonService.class);
    public static final String SCRIPTDIR = "/resources";
    private static final String SERVICE_R = "service.R";
    private static final int MAX = 2048000;

    @Context
    private ServletContext servletContext;

    @Inject
    private RConnectionPool rConnectionPool;

    private File toPhonemes(String text) throws Exception {
        String scriptDir = servletContext.getRealPath(SCRIPTDIR);
        WrappedRConnection rConnection = rConnectionPool.borrow();
        try {
            File tempFile = new File(UUID.randomUUID().toString());
            File phonemes = rConnection.initOutput();
            try {
                rConnection.assign(RconnectionFactory.INPUT_R_VAR_NAME, text);
                rConnection.eval("source(\"" + scriptDir + File.separator + SERVICE_R + "\")");
//                rConnection.assign(".tmp.", "source(\"" + scriptDir + File.separator + SERVICE_R + "\")");
//                REXP r = rConnection.parseAndEval("try(eval(parse(text=.tmp.)),silent=TRUE)");
//                if (r.inherits("try-error")) {
//                    System.err.println(((REXPString)r).toDebugString());
//                }
                Files.move(phonemes.toPath(), tempFile.toPath());
                return tempFile;
            } catch (RserveException | IOException ex) {
                tempFile.delete();
                ex.printStackTrace();
                throw ex;
            }
        } finally {
            rConnectionPool.putBack(rConnection);
        }
    }

    @GET
    @Path(INFOPATH)
    @Produces({MediaType.APPLICATION_JSON})
    public Response info() {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("version",versionInfo())
                .add("maxTextSize", MAX)
                .add(INFOPATH, "/"+RestApplication.ROOT+"/"+ROOT+"/"+ INFOPATH)
                .build();
        return Response.ok(jsonObject).build();
    }

    @POST
    @Path("process")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces("text/tab-separated-values")
    public Response tagTsv(@Size(min=1,max=MAX) String text) {
        return getResponse(text);
    }


    private Response getResponse(String text) {
        try {
            final StreamingOutput output = new StreamingOutput() {
                @Override
                public void write(OutputStream output) throws IOException, WebApplicationException {
                    try {
                        File tempFile = toPhonemes(text);
                        try {
                            Files.copy(tempFile.toPath(), output);
                        } finally {
                            tempFile.delete();
                        }
                    } catch (Exception ex) {
                        LOGGER.warn("unable to process",ex);
                        throw new WebApplicationException(
                                Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                                        deepestCause(ex).getMessage()).build());
                    }
                }
            };
            return Response.ok(output).build();
        } catch (WebApplicationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new WebApplicationException(
                    Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                            deepestCause(ex).getMessage()).build());
        }
    }

    public static Throwable deepestCause(Throwable t) {
        if (t.getCause() != null) {
            return deepestCause(t.getCause());
        }
        return t;
    }

    private static final Properties BUILDPROPERTIES = new Properties();

    static {
        try {
            BUILDPROPERTIES.load(Graph2PhonService.class.getResourceAsStream("/build.properties"));
        } catch (IOException e) {
            LOGGER.warn("unable to load build properties",e);
        }
    }

    public String versionInfo() {
        return BUILDPROPERTIES.toString();
    }

}
